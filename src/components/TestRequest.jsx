import React, {useState} from 'react';

const TestRequest = () => {
    const [currentQuestion, setCurrentQuestion] = useState(0)
    const [score, setScore] = useState(0)
    // 왜 만들고 있지? 목적
    // 사람들한테 문제 한번 풀어봐 경각심 일개워주고 점수 환산해서 병원 와 하고 꼬실라고
    const questions = [
        {
            question: "1. 변의 색깔은 어떠셨나요?",
            answers: [
                { text: "황갈색", score: 10 },
                { text: "붉은색", score: 0 },
                { text: "검정색", score: 0 },
                { text: "회백색", score: 0 },
                { text: "초록색", score: 5 },
                { text: "노란책", score: 5 }
            ]
        },
        {
            question: "2. 변의 형태는 어떠셨나요?",
            answers: [
                { text: "바나나 모양", score: 10 },
                { text: "가늘고 긴 모양", score: 5 },
                { text: "묽은 상태", score: 5 },
                { text: "자잘한 모양", score: 5 },
                { text: "끊긴 모양", score: 5 }
            ]
        },
        {
            question: "3. 변을 보는 시간은 어떻게 되나요?",
            answers: [
                { text: "5분 이내", score: 10 },
                { text: "10분 이내", score: 5 },
                { text: "15분 이내", score: 0 }
            ]
        },
        {
            question: "4. 변을 보는 빈도는 어떻게 되나요?",
            answers: [
                { text: "하루에 1~3번", score: 10 },
                { text: "2~3일에 한 번", score: 5 },
                { text: "4일에 한 번", score: 5 },
                { text: "일주일에 한 번", score: 0 },
                { text: "하루에 4 번 이상", score: 0 }
            ]
        },
        {
            question: "5. 변을 볼 때 통증이나 불편함을 느끼시나요?",
            answers: [
                { text: "없음", score: 10 },
                { text: "간혹 느낌", score: 5 },
                { text: "항상 느낌", score: 0 }
            ]
        },
        {
            question: "6. 일주일 평균 운동량은 어떻게 되시나요?",
            answers: [
                { text: "4회 이상", score: 10 },
                { text: "1 ~ 4회", score: 5 },
                { text: "0회", score: 0 }
            ]
        },
        {
            question: "7. 특정 약물을 복용하고 계신가요?",
            answers: [
                { text: "먹고 있다.", score: 0 },
                { text: "먹고 있지 않다.", score: 10 }
            ]
        }
    ]
    const getResult = score => {
        if (score >= 65) return "당신은 매우 건강한 장을 가지고 있습니다."
        else if (score >= 30) return "지금은 괜찮으나 관리를 잘해주세요"
        else return "빨리 시급히 병원을 방문해주세요"
    }

    const handleAnswerClick = answerScore => {
        setScore(score + answerScore) // 셋스코어는 스코어 + 앤서스코어
        const nextQuestion = currentQuestion + 1 // 변수를 선언하는데 넥스트퀘스천 이라고 짓고 안에는 커런트퀘스천 +1 를 넣는다.
        if (nextQuestion < questions.length) { // if 위에서 선언한 넥스트 퀘스천이 퀘스천에 랭스보다 작으면 참이다.
            setCurrentQuestion(nextQuestion) // 참일 경우 셋커런트퀘스천에 넥스트퀘스천을 담아준다.
        } else {
            alert(getResult(score + answerScore)) // 거짓일 경우 변수 메세지를 출력하는 함수안에 겟리절트라는 변수를 넣어주는데 겟이러트는 if문으로 score 점수로 판별해서 리턴값을 준다.
        }
    }

    return (
        <div>
            {/*퀘스천 변수에서 커런트 퀘스천 현재번호 . 퀘스천을 가져온다.*/}
            <h2>{questions[currentQuestion].question}</h2>
            {/*퀘스천 변수에서 커런트 퀘스천 현재번호 . 앤서를 map 으로 돌려서 반복한다. */}
            {questions[currentQuestion].answers.map((answer) => (
                //
                <button key={answer.text} onClick={() => handleAnswerClick(answer.score)}>
                    {answer.text}
                </button>
            ))}
        </div>
    )
}

export default TestRequest;